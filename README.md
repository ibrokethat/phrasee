# Phrasee test

Please accept my apologies, but I have only done the back end due to time constraints. You will just have to imagine that there is a well designed react app that renders the feed, consisting of pure rendering components and loading hooks that would likely have been using `tanstack query`. 

My plan had been to add the react app to the docker compose file, and sit it in front of the the nginx server to call one of the three aggregator instances.

## To run the app: 

```
git clone git@gitlab.com:ibrokethat/phrasee.git
cd phrasee
docker compose up
```

The aggregator API is exposed at `http://localhost:3000/feed`

It's spawning 3 intances of the container and serving it from behind nginx. Why, well, um, er, why not?

## To run the tests:

```
cd aggregator
npm i
npm run test
npm run test:int
```

## Development server:

The development server is exposed at `http://localhost:4000/feed`

```
cd aggregator
npm i
npm run build
npm run dev
```

### Notes on the choices I made:

I used a run time type library, zod in this case, to parse all data coming into the app. This ensures strong data integrity/typing throughout.

I went with express to keep things simple, as opposed to a heavy framework like nest which would be overkill. This also meant I could show how I think about application structure. Whether you agree with my choices or not, separation of concerns have been achieved by piping data through a flow where each step is represented by single function, eg: `data loading -> data validation -> data transformation`. These are also located separately on the filesystem.

Keeping things as functional as possible using composition helps keep the code readable, concise, and easily testable.

I set the code coverage to be 100% on the domain, services and lib directories. Http route handling is tested via the integration test.

I structured the output of the aggregated feed to capture what look like distinct elements of data within the graphic representation. This would allow a UI to display variants of the data without requiring separate feeds.

To demonstrate decoupling configuration from the application - there is a fake s3 endpoint being passed into the application via environment variables. This value would be provided at deployment time VIA a ci/cd 
pipeline in real life.

### Future considerations:

As this particular feed didn't require request parameters, I didn't implement request body/parameter/query parsing. If needed I would have augmented the generic handler and pipeline to accept parameters which would have been validated using `zod-express-middleware`.

The generic route handler could be improved to take a fallback function that returned default data under certain error conditions.

If there were more than one endpoint I would abstract the route binding out from the app.mts file.

I didn't implement hot loading when running the development server. I would in real life.

I would generate an open api spec for production.

I wouldn't just chuck everything into git with one giant commit in real life like I did here.

# The most important part of the README:

If I were asked to build this API of aggregated feed data in real life, I would not choose to build the aggregated data set at request time in this manner.

I would persist the stream of events, Likes, Comments, etc, to a data store, and trigger background processes that would update a secondary data store with the aggregated feed when these events are written. I would serve the feed from this secondary data set. This would give greater scope to order the feeds posts via the most recently liked or commented event (or any other criteria), and have the added benefit of greatly simplifying the aggregation code.