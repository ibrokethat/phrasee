import supertest from 'supertest';
import { z } from 'zod';
import { app, server } from '../src/app.mjs';
 
describe('basic integration test', () => {

  //   todo: exception tests

  afterAll(() => {
    server.close();
  })

  describe('success', () => {    
    
    it('returns the aggregated feed', async () => {
      
      const apiResponseSchema = z.array(
        z.object({
          postId: z.string(),
          who: z.string(),
          action: z.string(),
          what: z.string(),
          text: z.string(),
          avatarUri: z.string().optional(),
        })
      );

      const res = await supertest(app)
        .get('/feed');
      expect(res.statusCode).toEqual(200);
      expect(() => apiResponseSchema.parse(res.body)).not.toThrow();
      
    });
  });
});

