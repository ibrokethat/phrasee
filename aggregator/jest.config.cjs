/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
/*
 * For a detailed explanation regarding each configuration property, visit:
 * https://jestjs.io/docs/configuration
 */

const {
    env,
  } = require('node:process');
  
  module.exports = {
    clearMocks: true,
    collectCoverage: env.CI !== 'true',
    collectCoverageFrom: [
      './src/**/*.{js,jsx,mjs,mjsx,mts,mtsx,ts,tsx}',
    ],
    coveragePathIgnorePatterns: [
      'types\.mts$',
      'index\.mts$',
      'app\.mts$',
      '/coverage/',
      '/dist/',
      '/node_modules/',
      '/__tests__/',
    ],
    coverageProvider: 'v8',
    coverageReporters: [
      'json',
      'lcov',
      'text',
      'text-summary',
      'clover',
    ],
    coverageThreshold: {
      global: {
        branches: 100,
        functions: 100,
        lines: 100,
        statements: 100,
      },
    },
  
    extensionsToTreatAsEsm: [ '.mts' ],
    moduleFileExtensions: [ 'mts', 'mjs', 'ts', 'js', 'json', 'node' ],
    preset: 'ts-jest/presets/js-with-babel-esm',
    resolver: '<rootDir>/esmodule-resolver.ts',
    rootDir: __dirname,
    roots: [
      '<rootDir>/src',
    ],
    setupFiles: [ '<rootDir>/jest.env.vars.js' ],
    silent: false,
    testEnvironment: 'node',
    testPathIgnorePatterns: [ '/node_modules/', '.d.ts', '.js', '.snap' ],
    testRegex: '\\.(spec|test)\\.m?ts?',
    transform: {
      '\\.m?[tj]sx?$': 'ts-jest',
    },
    transformIgnorePatterns: [ 'node_modules/(?!(abort-controller|event-target-shim)/)' ],
    verbose: true,
  };
  