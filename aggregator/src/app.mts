import express from 'express';
import { pinoHttp } from 'pino-http';

import { pipeline } from './domain/index.mjs';
import { makeHandler } from './lib/make-handler.mjs';

const logger = pinoHttp();
const app = express();

app.use(logger);

app.get(
  '/feed',
  makeHandler(pipeline),
);

const server = app.listen(4000);

export {
  app, server
}