import { env } from 'node:process';
import { z } from 'zod';

const envVarsSchema = z.object({
  FAKE_S3_ENDPOINT: z.string(),
});

const envVars = envVarsSchema.parse(env);

export default envVars;
