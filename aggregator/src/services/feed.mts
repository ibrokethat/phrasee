import { readFile } from 'node:fs/promises'; 
import { resolve } from 'node:path'; 

import { Logger } from 'pino';

export const feed = async (logger: Logger) => {
  try {
    const res = await readFile(resolve('notifications-feed.json'), 'utf-8');
    return JSON.parse(res);
  } catch (err) {
    const e = {
      type: 'ServiceError',
      err
    };
    logger.error(e, 'feed failed to load');
    throw e;
  }
}