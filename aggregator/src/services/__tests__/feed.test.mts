import fs from 'node:fs/promises'; 

import { Logger } from 'pino';
import { feed } from '../feed.mjs'

describe('feed', () => {    
    
  it('should parse the raw input according to the provided schema', async () => {
    jest.spyOn(fs, 'readFile').mockResolvedValueOnce(JSON.stringify({
      name: 'John Doe',
      age: 30,
    }));

    const logger = { error: jest.fn() } as unknown as Logger;
    const result = await feed(logger);

    expect(result).toEqual({
      name: 'John Doe',
      age: 30,
    });
    expect(logger.error).not.toHaveBeenCalled();
  });

  it('should throw an InvalidDataError error when validation fails', async () => {

    jest.spyOn(fs, 'readFile').mockResolvedValueOnce('{\'invalid json\': \'value\'}');
    const logger = { error: jest.fn() } as unknown as Logger;
    
    await expect(() => feed(logger)).rejects.toEqual(expect.objectContaining({type: 'ServiceError'}));
    expect(logger.error).toHaveBeenCalledWith(
      expect.objectContaining({type: 'ServiceError'}), 
      'feed failed to load'
    )
  });
});

