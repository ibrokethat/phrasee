import { Logger } from 'pino';
import { makePipeline } from '../make-pipeline.mjs'

describe('make pipeline', () => {      
  it('should run the pipeline', async () => {

    const service = jest.fn().mockResolvedValue({
      name: 'John Doe',
      age: 30,
    });
    const validator = jest.fn().mockReturnValue({
      name: 'John Doe',
      age: 30,
    });
    const transformer = jest.fn().mockReturnValue({
      firstName: 'John',
      lastName: 'Doe',
    });

    const pipeline = makePipeline(
      service,
      validator,
      transformer,
    );
  
    const logger = { error: jest.fn() } as unknown as Logger;
    const result = await pipeline(logger);

    expect(service).toHaveBeenCalledWith(logger);
    expect(validator).toHaveBeenCalledWith(logger, {
      name: 'John Doe',
      age: 30,
    });
    expect(transformer).toHaveBeenCalledWith(logger, {
      name: 'John Doe',
      age: 30,
    });

    expect(result).toEqual({
      firstName: 'John',
      lastName: 'Doe',
    });

    expect(logger.error).not.toHaveBeenCalled();
  });

});
