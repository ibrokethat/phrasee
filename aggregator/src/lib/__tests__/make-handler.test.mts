import { createRequest, createResponse } from 'node-mocks-http';

import { Logger } from 'pino';
import { makeHandler } from '../make-handler.mjs'

describe('make handler', () => {      
  it('should handle a request', async () => {

    const pipeline = jest.fn().mockResolvedValue({
      name: 'John Doe',
      age: 30,
    });
    const handler = makeHandler(
      pipeline
    );
  
    const req = createRequest();
    req.log = { error: jest.fn() } as unknown as Logger;
    const res = createResponse();
        
    await handler(req, res);

    const result = res._getJSONData();

    expect(pipeline).toHaveBeenCalledWith(req.log);    
    expect(res.statusCode).toEqual(200);
    expect(result).toEqual({
      name: 'John Doe',
      age: 30,
    });

    expect(req.log.error).not.toHaveBeenCalled();
  });

  it('should return a 500 under', async () => {

    const pipeline = jest.fn().mockRejectedValue({
      type: 'Error',
    });
    const handler = makeHandler(
      pipeline
    );
  
    const req = createRequest();
    req.log = { error: jest.fn() } as unknown as Logger;
    const res = createResponse();
        
    await handler(req, res);

    expect(pipeline).toHaveBeenCalledWith(req.log);    
    expect(res.statusCode).toEqual(500);

    expect(req.log.error).toHaveBeenCalledWith(expect.objectContaining({type: 'RouteHandlerError'}), 'request failed');
  });


});
