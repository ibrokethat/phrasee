import { Logger } from 'pino';
import { z } from 'zod';
import { makeValidator } from '../make-validator.mjs'

describe('make validator', () => {    
  const schema = z.object({
      name: z.string(),
      age: z.number(),
  });
  const validate = makeValidator(schema);
  
  it('should parse the raw input according to the provided schema', () => {
    const logger = { error: jest.fn() } as unknown as Logger;
    const result = validate(logger, {
      name: 'John Doe',
      age: 30,
    });

    expect(result).toEqual({
      name: 'John Doe',
      age: 30,
    });
    expect(logger.error).not.toHaveBeenCalled();
  });

  it('should throw an InvalidDataError error when validation fails', () => {
    const logger = { error: jest.fn() } as unknown as Logger;
    
    expect(() => validate(logger, {
      age: 30,
    })).toThrow(expect.objectContaining({type: 'InvalidDataError'}));
    expect(logger.error).toHaveBeenCalledWith(
      expect.objectContaining({type: 'InvalidDataError'}), 
      'data validation failed'
    )
  });
});
