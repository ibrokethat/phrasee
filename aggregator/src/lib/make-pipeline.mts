import { z } from 'zod';
import { Logger } from 'pino';

type Service = (logger: Logger) => Promise<unknown>;
type Validator<Input> = (logger: Logger, raw: unknown) => Input;
type Transformer<Input, Output> = (logger: Logger, items: Input) => Output;
type Pipeline<Output> = (logger: Logger) => Promise<Output>;
type MakePipeline = <InputSchema extends z.ZodTypeAny, Output>(
  service: Service,
  validate: Validator<z.TypeOf<InputSchema>>,
  transform: Transformer<z.TypeOf<InputSchema>, Output>,
) => Pipeline<Output>;

export const makePipeline: MakePipeline = (service, validate, transform) => async (logger) => {
  const raw = await service(logger);
  const data = validate(logger, raw);
  return transform(logger, data);
};
