import { Request, Response } from 'express';
import { Logger } from 'pino';

type Pipeline<Output> = (logger: Logger) => Promise<Output>;
type Handler<Output> = (req: Request, res: Response<Output>) => Promise<void>;
type MakeHandler = <Output>(
  pipeline: Pipeline<Output>,
) => Handler<Output>;

export const makeHandler: MakeHandler = (pipeline) => async (req, res) => {
    try {
      const result = await pipeline(req.log);
      res.status(200).json(result);
    } catch (err) {      
      req.log.error({type: 'RouteHandlerError', err}, 'request failed');
      res.status(500).send();
    }
  };
