import { Logger } from 'pino';
import { z } from 'zod';

type Validator<Schema> = (logger: Logger, raw: unknown) => Schema;
type MakeValidator = <
  Schema extends z.ZodTypeAny,
>(schema: Schema) => Validator<z.infer<Schema>>;

export const makeValidator: MakeValidator = (schema ) => (logger, raw) => {
  try {
    return schema.parse(raw);
  } catch (err) {
    const e = {
      data: raw,
      err,
      type: 'InvalidDataError',
    }
    logger.error(e, 'data validation failed');
    throw e; 
  }
}
