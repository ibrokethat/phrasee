import { feed } from '../services/feed.mjs'

import { transform } from './transformer.mjs';

import { feedSchema } from './types.mjs'
import type { AggregatedFeedItem } from './types.mjs'

import { makePipeline } from '../lib/make-pipeline.mjs';
import { makeValidator } from '../lib/make-validator.mjs';

export const pipeline = makePipeline<typeof feedSchema, AggregatedFeedItem[]>(
  feed,
  makeValidator(feedSchema),
  transform
);
