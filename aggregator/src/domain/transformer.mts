import { Logger } from 'pino';

import { formatComment, formatLike } from './formatters.mjs';
import type { AggregatedFeedItem, CommentItem, Feed, LikeItem } from './types.mjs';

export const transform = (logger: Logger, feed: Feed): AggregatedFeedItem[] => {
  try {    
    const initial: Record<string, { comments: CommentItem[], likes: LikeItem[] }> = {};      
    const postsByAction = feed.reduce((acc, item) => {
      if (!acc[item.post.id]) {
        acc[item.post.id] = {
          comments: [],
          likes: [],
        }
      }
      
      if (item.type === 'Comment') {
        acc[item.post.id].comments.push(item);
      }
      if (item.type === 'Like') {
        acc[item.post.id].likes.push(item);
      }

      return acc;
    }, initial)

    return Object.entries(postsByAction).flatMap(( [id, post] ) => [
      ...formatComment(id, post.comments),
      ...formatLike(id, post.likes)
    ]);

  } catch (err) {
    const e = {
      data: feed,
      err,
      type: 'DataTransformError',
    };
    logger.error(e, 'feed aggregation failed');
    throw e;
  }
}