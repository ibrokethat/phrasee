import { Logger } from 'pino';

import { transform } from '../transformer.mjs';
import type { Feed } from '../types.mjs';

import testFeed from './test-feed.json';

describe('transformer', () => {
  it('aggregates all the feed items correctly', () => {
    const logger = { error: jest.fn() } as unknown as Logger;
    const aggregatedFeed = transform(
      logger,
      testFeed as Feed
    );
    
    expect(aggregatedFeed).toHaveLength(6);
    expect(aggregatedFeed).toEqual(expect.arrayContaining([
      {
        action: 'commented',
        avatarUri: 's3-endpoint/portrait_05.png',
        postId: 'b1638f970c3ddd528671df76c4dcf13e',
        text: 'Acme remains one of my fave company ever! The way they scale is so dynamic that makes HTML5 look static!',
        what: 'on your post',
        who: 'Suoma Narjus',
      },
      {
        action: 'liked',
        avatarUri: undefined,
        postId: 'b1638f970c3ddd528671df76c4dcf13e',
        text: 'Acme Inc dynamically scales niches worldwide',
        what: 'your post',
        who: 'Mary T. Price, Suoma Narjus and 3 others',
      },
      {
        action: 'liked',
        avatarUri: 's3-endpoint/portrait_01.png',
        postId: '7d78ff348647b782cb3027d836d23e09',
        text: 'How to professionally administrate seamless growth strategies in 10 steps',
        what: 'your post',
        who: 'Bojana Novaković, Mr Smartypants and 4 others',
      },
      {
        action: 'commented',
        avatarUri: 's3-endpoint/portrait_02.png',
        postId: 'c4cfbe322bb834ada81719036f9b287b',
        text: 'How to distinctively leverage existing wireless ROI',
        what: 'on your post',
        who: 'Ali Sage, Harold Lachlan and 1 other',
      },
      {
        action: 'likes',
        avatarUri: 's3-endpoint/portrait_08.png',
        postId: 'de8b75335ba7e52e62c8227c6697def2',
        text: 'This company enthusiastically deployed extensive values, the rest is history',
        what: 'your post',
        who: '',
      },
      {
        action: 'commented',
        avatarUri: 's3-endpoint/portrait_14.png',
        postId: '57e0d6328c9287bd1b66bc327efbcdfa',
        text: 'Here we go again!',
        what: 'on your post',
        who: 'Chuck Looij',
      },
    ]));
    expect(logger.error).not.toHaveBeenCalled();
  });

  it('doesn\'t process comments when there are none', () => {
    const logger = { error: jest.fn() } as unknown as Logger;
    const aggregatedFeed = transform(
      logger,
      testFeed.filter((item) => item.type === 'Like') as Feed
    );

    expect(aggregatedFeed).toHaveLength(3);
    expect(aggregatedFeed).toEqual(expect.arrayContaining([
      {
        action: 'liked',
        avatarUri: undefined,
        postId: 'b1638f970c3ddd528671df76c4dcf13e',
        text: 'Acme Inc dynamically scales niches worldwide',
        what: 'your post',
        who: 'Mary T. Price, Suoma Narjus and 3 others',
      },
      {
        action: 'liked',
        avatarUri: 's3-endpoint/portrait_01.png',
        postId: '7d78ff348647b782cb3027d836d23e09',
        text: 'How to professionally administrate seamless growth strategies in 10 steps',
        what: 'your post',
        who: 'Bojana Novaković, Mr Smartypants and 4 others',
      },
      {
        action: 'likes',
        avatarUri: 's3-endpoint/portrait_08.png',
        postId: 'de8b75335ba7e52e62c8227c6697def2',
        text: 'This company enthusiastically deployed extensive values, the rest is history',
        what: 'your post',
        who: '',
      },
    ]));
    expect(logger.error).not.toHaveBeenCalled();
  });

  it('doesn\'t process likes when there are none', () => {
    const logger = { error: jest.fn() } as unknown as Logger;
    const aggregatedFeed = transform(
      logger,
      testFeed.filter((item) => item.type === 'Comment') as Feed
    );
    
    expect(aggregatedFeed).toHaveLength(3);
    expect(aggregatedFeed).toEqual(expect.arrayContaining([
      {
        action: 'commented',
        avatarUri: 's3-endpoint/portrait_05.png',
        postId: 'b1638f970c3ddd528671df76c4dcf13e',
        text: 'Acme remains one of my fave company ever! The way they scale is so dynamic that makes HTML5 look static!',
        what: 'on your post',
        who: 'Suoma Narjus',
      },
      {
        action: 'commented',
        avatarUri: 's3-endpoint/portrait_02.png',
        postId: 'c4cfbe322bb834ada81719036f9b287b',
        text: 'How to distinctively leverage existing wireless ROI',
        what: 'on your post',
        who: 'Ali Sage, Harold Lachlan and 1 other',
      },
      {
        action: 'commented',
        avatarUri: 's3-endpoint/portrait_14.png',
        postId: '57e0d6328c9287bd1b66bc327efbcdfa',
        text: 'Here we go again!',
        what: 'on your post',
        who: 'Chuck Looij',
      },
    ]));
    expect(logger.error).not.toHaveBeenCalled();
  });

  it('should throw a data transform exception when catching an error', () => {
    const logger = { error: jest.fn() } as unknown as Logger;
    
    expect(() => transform(
      logger,
      {} as unknown as Feed
    )).toThrow(expect.objectContaining({type: 'DataTransformError'}));
    expect(logger.error).toHaveBeenCalledWith(
      expect.objectContaining({type: 'DataTransformError'}), 
      'feed aggregation failed'
    )


  });


});

  