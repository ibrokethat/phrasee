import  env from '../env.mjs';
import type { AggregatedFeedItem, CommentItem, LikeItem, User } from './types.mjs';


const formatWho = (users: User[]) => {
  const [first, second, ...others] = users;
  //  eeuuugghhh :)
  return `${first.name}${second ? `, ${second.name}` : ''}${others.length ? ` and ${others.length} other${others.length > 1 ? `s` : ''}` : ''}`
}

const formatAvatarUri = (user: User) => {
  return user.avatar ? `${env.FAKE_S3_ENDPOINT}/${user.avatar}` : undefined
}

export const formatComment = (postId: string, comments: CommentItem[]): AggregatedFeedItem[] => {
  
  if (!comments.length) {
    return [];
  }    

  return [{
    postId,
    action: 'commented',
    text: comments.length === 1 ? comments[0].comment.commentText : comments[0].post.title,
    what: 'on your post',
    who: formatWho(comments.map(({user}) => user)),
    avatarUri: formatAvatarUri(comments[0].user)
  }];
}

export const formatLike = (postId: string, likes: LikeItem[]): AggregatedFeedItem[] => {

  if (!likes.length) {
    return [];
  }    

  return [{
    postId,
    action: likes.length === 1 ? 'likes' : 'liked',
    text: likes[0].post.title,
    what: 'your post',
    who: formatWho(likes.map(({user}) => user)),
    avatarUri: formatAvatarUri(likes[0].user)
  }]
}