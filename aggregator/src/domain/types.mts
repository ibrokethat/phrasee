import { z } from 'zod';

export const userSchema = z.object({ 
  avatar: z.string().optional(),
  id: z.string(), 
  name: z.string(),
});
export type User = z.infer<typeof userSchema>;

const postSchema = z.object({ 
  id: z.string(), 
  title: z.string(), 
});
export type Post = z.infer<typeof postSchema>;

const commentSchema = z.object({ 
  commentText: z.string(),
  id: z.string(), 
});
export type Comment = z.infer<typeof commentSchema>;


const commentItemSchema = z.object({
  type: z.literal('Comment'),
  comment: commentSchema,
  post: postSchema,
  user: userSchema,
});
export type CommentItem = z.infer<typeof commentItemSchema>;

const likeItemSchema = z.object({
  type: z.literal('Like'),
  post: postSchema,
  user: userSchema,
});
export type LikeItem = z.infer<typeof likeItemSchema>;

export const feedItemSchema = z.union([
  commentItemSchema,
  likeItemSchema,
]);
export type FeedItem = z.infer<typeof feedItemSchema>;

export const feedSchema = z.array(feedItemSchema);
export type Feed = z.infer<typeof feedSchema>;

export type AggregatedFeedItem = {
  postId: string;
  who: string;
  action: 'liked' | 'likes' | 'commented';
  what: string;
  text: string;
  avatarUri?: string;
};