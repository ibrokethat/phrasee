/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
/*
 * For a detailed explanation regarding each configuration property, visit:
 * https://jestjs.io/docs/configuration
 */

const {
    env,
  } = require('node:process');
  
  module.exports = {
    clearMocks: true,
    extensionsToTreatAsEsm: [ '.mts' ],
    moduleFileExtensions: [ 'mts', 'mjs', 'ts', 'js', 'json', 'node' ],
    preset: 'ts-jest/presets/js-with-babel-esm',
    resolver: '<rootDir>/esmodule-resolver.ts',
    rootDir: __dirname,
    roots: [
      '<rootDir>/int-tests',
    ],
    setupFiles: [ '<rootDir>/jest.env.vars.js' ],
    silent: false,
    testEnvironment: 'node',
    testPathIgnorePatterns: [ '/node_modules/', '.d.ts', '.js', '.snap' ],
    testRegex: '\\.(spec|test)\\.m?ts?',
    transform: {
      '\\.m?[tj]sx?$': 'ts-jest',
    },
    transformIgnorePatterns: [ 'node_modules/(?!(abort-controller|event-target-shim)/)' ],
    verbose: true,
  };
  